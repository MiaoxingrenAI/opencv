
#include <iostream>

#include "opencv2/opencv_modules.hpp"
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/cudafeatures2d.hpp"
#include "opencv2/xfeatures2d/cuda.hpp"

using namespace std;
using namespace cv;
using namespace cv::cuda;



#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;
using namespace cv::cuda;



int main()
{
	//测试用例1
	/*-------------------------以下四种验证方式任意选取一种即可-------------------------*/
	//获取显卡简单信息
	cuda::printShortCudaDeviceInfo(cuda::getDevice());  //有显卡信息表示GPU模块配置成功

	//获取显卡详细信息
	cuda::printCudaDeviceInfo(cuda::getDevice());  //有显卡信息表示GPU模块配置成功

	//获取显卡设备数量
	int Device_Num = cuda::getCudaEnabledDeviceCount();
	cout << Device_Num << endl;  //返回值大于0表示GPU模块配置成功

	//获取显卡设备状态
	cuda::DeviceInfo Device_State;
	bool Device_OK = Device_State.isCompatible();
	cout << "Device_State: " << Device_OK << endl;  //返回值大于0表示GPU模块配置成功

	system("pause");



	//测试用例2

	GpuMat img1, img2;
	img1.upload(imread("graf1.png", IMREAD_GRAYSCALE));
	img2.upload(imread("graf3.png", IMREAD_GRAYSCALE));

	cv::cuda::printShortCudaDeviceInfo(cv::cuda::getDevice());

	SURF_CUDA surf;

	// detecting keypoints & computing descriptors
	GpuMat keypoints1GPU, keypoints2GPU;
	GpuMat descriptors1GPU, descriptors2GPU;
	surf(img1, GpuMat(), keypoints1GPU, descriptors1GPU);
	surf(img2, GpuMat(), keypoints2GPU, descriptors2GPU);

	cout << "FOUND " << keypoints1GPU.cols << " keypoints on first image" << endl;
	cout << "FOUND " << keypoints2GPU.cols << " keypoints on second image" << endl;

	// matching descriptors
	Ptr<cv::cuda::DescriptorMatcher> matcher = cv::cuda::DescriptorMatcher::createBFMatcher(surf.defaultNorm());
	vector<DMatch> matches;
	matcher->match(descriptors1GPU, descriptors2GPU, matches);

	// downloading results
	vector<KeyPoint> keypoints1, keypoints2;
	vector<float> descriptors1, descriptors2;
	surf.downloadKeypoints(keypoints1GPU, keypoints1);
	surf.downloadKeypoints(keypoints2GPU, keypoints2);
	surf.downloadDescriptors(descriptors1GPU, descriptors1);
	surf.downloadDescriptors(descriptors2GPU, descriptors2);

	// drawing the results
	Mat img_matches;
	drawMatches(Mat(img1), keypoints1, Mat(img2), keypoints2, matches, img_matches);

	namedWindow("matches", 0);
	imshow("matches", img_matches);
	//出现关键点匹配结果，说明cuda配置成功
	waitKey(0);

	return 0;
}
